#include <stdarg.h>
#include "util.h"
#include "test.h"

void ensure(bool condition, const char *msg) {
    if (!condition) err(msg);
}

void test1(void *heap) {
    uint8_t* mem = (uint8_t*) _malloc(BLOCK_SIZE, heap);
    ensure(mem, "Не удалось выделить память\n");
    ensure(block_get_header(mem)->capacity.bytes == BLOCK_SIZE, "\n");
    size_t bytes = 0;
    for (struct block_header* header = block_get_header(mem); header && !header->is_free; header = header->next) {
        bytes += header->capacity.bytes;
    }
    ensure(bytes == BLOCK_SIZE, "Суммарный размер последовательных блоков не совпадает с размером участка\n\n");
}

void test2(void *heap) {
    uint8_t* mem1 = (uint8_t*) _malloc(BLOCK_SIZE, heap);
    uint8_t* mem2 = (uint8_t*) _malloc(BLOCK_SIZE, heap);
    uint8_t* mem3 = (uint8_t*) _malloc(BLOCK_SIZE, heap);
    _free(mem2);
    ensure(!block_get_header(mem1)->is_free, "Участок mem1 должен был освободиться\n");
    ensure(block_get_header(mem2)->is_free, "Участок mem2 не должен был освободиться\n");
    ensure(!block_get_header(mem3)->is_free, "Участок mem3 должен был освободиться\n");
}

void test3(void *heap) {
    uint8_t* mem1 = (uint8_t*) _malloc(BLOCK_SIZE, heap);
    uint8_t* mem2 = (uint8_t*) _malloc(BLOCK_SIZE, heap);
    uint8_t* mem3 = (uint8_t*) _malloc(BLOCK_SIZE, heap);
    _free(mem1);
    _free(mem2);
    ensure(block_get_header(mem1)->is_free, "Участок mem1 должен был освободиться\n");
    ensure(block_get_header(mem2)->is_free, "Участок mem2 должен был освободиться\n");
    ensure(!block_get_header(mem3)->is_free, "Участок mem3 не должен был освободиться\n");
}

void test4(void *heap) {
    // т.к. начальный размер кучи равен REGION_MIN_SIZE произойдет создание нового региона
    uint8_t* mem = (uint8_t*) _malloc(REGION_MIN_SIZE * 2, heap);
    ensure(mem, "Не удалось выделить память");
    //ensure(block_get_header(mem)->capacity.bytes == REGION_MIN_SIZE * 2, "Суммарный размер последовательных блоков не совпадает с размером участка\n");
}

void test5(void *heap) {
    // Выделяется три участка памяти : mem1, mem2, mem3
    // Затем mem1 освобождается и, происходить попытка выделить вместо него бОльший участок
    // Поскольку размер блока под старым mem1 меньше требуемого, под новый участок будет в регион памяти,
    // расположенный после mem2 и mem3
    uint8_t* mem1 = (uint8_t*) _malloc(REGION_MIN_SIZE / 2, heap);
    uint8_t* mem2 = (uint8_t*) _malloc(REGION_MIN_SIZE / 2, heap);
    uint8_t* mem3 = (uint8_t*) _malloc(REGION_MIN_SIZE, heap);
    _free(mem1);
    mem1 = (uint8_t*) _malloc(REGION_MIN_SIZE, heap);
    size_t bytes = 0;
    for (struct block_header* header = block_get_header(mem1); header && !header->is_free; header = header->next) {
        ensure(header->contents != mem2, "Память должна была выделиться после участка mem2\n");
        ensure(header->contents != mem3, "Память должна была выделиться после участка mem3\n");
        bytes += header->capacity.bytes;
    }
    ensure(bytes == REGION_MIN_SIZE, "Суммарный размер последовательных блоков не совпадает с размером участка\n");
}

void test_all(void (*test)(void*), ...) {
    va_list args;
    va_start (args, test);
    for (int i = 1; test; i++) {
        void *heap = heap_init(REGION_MIN_SIZE);
        printf("# Тест %d : ", i);
        fflush(stdout);
        test(heap);
        printf(" Успех\n");
        test = va_arg(args, void (*)(void*));
    }
    va_end (args);
}
