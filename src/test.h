#include "mem_internals.h"
#include "mem.h"

#define BLOCK_SIZE 1024

void ensure(bool condition, const char *msg);

void test1(void *heap);

void test2(void *heap);

void test3(void *heap);

void test4(void *heap);

void test5(void *heap);

void test_all(void (*test)(void*), ...);
